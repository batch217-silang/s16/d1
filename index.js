// SECTION Arithmetic Operators = +,-,*,%(modulus- for determining if the number is even or odd)/

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);
let difference = x - y;
console.log("Result of subtraction operator: " + difference);
let quotient = x / y;
console.log("Result of division operator: " + quotient);
let product = x * y;
console.log("Result of multiplication operator: " + product);
let remainder = y % x;
console.log("Result of Module operator: " + remainder)

// SECTION Assignment operator
// Basic assignment operator is (=) equal sign.
let assignmentNumber = 8;
// addition assignment
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " +assignmentNumber); 

// shortcut version  (+=,-=.*=,/=)
assignmentNumber += 2;
console.log("Result of addition assignment operator: " +assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " +assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " +assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " +assignmentNumber);

// Multiple Operators and Parenthesis
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// INCREMENTATION(+1) and DECREMENTATION(-1)
let z = 1;
// pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of post-increment: " + z);
// post-increment
increment = z++;
// increment = z++;
console.log("Result of post-increment: " + increment);
// decrementation
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// SECTION - TYPE COERCION - automatic conversion of values from one data type to another
let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let noncoercion = numC + numD;
console.log(noncoercion);
console.log(typeof noncoercion);

// true=1
let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// false=0
let numF = false + 1;
console.log(numF);
console.log(typeof numF);

// COMPARISON OPERATOR
let juan = "juan";

// equality operator (==)
// = assignment of value
// == comparison equality

console.log(1 == 1);
console.log(1 == 2);
console.log(0 == 1);
console.log('1' == 1);
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);

// Inequality operator (!=)
// ! = not
console.log(1 != 1);
console.log(1 != 2);


// Strict Equality Operator (===)
// checks if the value or operand are equal of the SAME TYPE
console.log('1' === 1); 
console.log('juan' === juan);
console.log(0 === false);


// Strict Inequality Operator (!==)
console.log('1' !== 1); 
console.log('juan' !== juan);
console.log(0 !== false);

// RELATIONAL OPERATOR >,<,=
// checks whether one value is greater than or less than 
let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);


let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// LOGICAL OPERATORS
// && = and, || = or, ! = not

let isLegalAge = true;
let isRegistered = false;
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result:" + allRequirementsMet)

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical || Result:" + someRequirementsMet)

let someRequirementsNotMet = !isRegistered
console.log("Logical NOT Result:" + someRequirementsNotMet)